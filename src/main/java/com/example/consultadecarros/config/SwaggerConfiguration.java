package com.example.consultadecarros.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


// @Configuration
// @EnableSwagger2
// public class SwaggerConfiguration {
    
//     private static final String TITLE = "Flight API";
//     private static final String DESCRIPTION = "Descripcion API Flight";
//     private static final String BASE_PACKAGE = "com.hiberus.flight.controller";
//     private static final String VERSION = "v1";

//     @Bean
//     public Docket swagger() {
//         return new Docket(DocumentationType.SWAGGER_2)
//                 .select()
//                 .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
//                 .build()
//                 .forCodeGeneration(true)
//                 .apiInfo(new ApiInfoBuilder().title(TITLE).description(DESCRIPTION).version(VERSION).build());
//     }
// }

// @Configuration
// @EnableSwagger2
// public class SwaggerConfiguration {

//     private static final String TITLE = "Flight API";
//     private static final String DESCRIPTION = "Descripcion API Flight";
//     private static final String BASE_PACKAGE = "com.hiberus.flight.controller";
//     private static final String VERSION = "v1";

//     @Bean
//     public Docket api() {
//         return new Docket(DocumentationType.SWAGGER_2)
//                 .select()
//                 .apis(RequestHandlerSelectors.any())
//                 .paths(PathSelectors.any())
//                 .build()
//                 .apiInfo(apiInfo());
//     }

//     private ApiInfo apiInfo() {
//         return new ApiInfoBuilder().title(TITLE).description(DESCRIPTION).version(VERSION).build();
//     }
// }

// @Configuration
// @EnableSwagger2
// public class SwaggerConfig {

//     @Autowired
// 	BuildProperties buildProperties;
    

//     @Bean
//     public Docket api() {
//         return new Docket(DocumentationType.SWAGGER_2)
//                 .select()
//                 .apis(RequestHandlerSelectors.any())
//                 .paths(PathSelectors.any())
//                 .build()
//                 .apiInfo(apiInfo());
//     }

//     private ApiInfo apiInfo() {
//         return new ApiInfoBuilder()
//                 .title("Documentação REST API")
//                 .description("Documentação para a API do CONSULTA-CARROS")
//                 // .contact(new Contact("Jefferson Sousa", null, "jeffersonlucindo@gmail.com"))
//                 .version(buildProperties.getVersion())
//                 .build();
//     }
//     // @Bean
//     // public Docket api() { 
//     //     return new Docket(DocumentationType.SWAGGER_2)  
//     //       .select()                                  
//     //       .apis(RequestHandlerSelectors.any())              
//     //       .paths(PathSelectors.any())                          
//     //       .build();             
//     // }
// }

// @Configuration
// public class SpringFoxConfig {                                    
//     @Bean
//     public Docket api() { 
//         return new Docket(DocumentationType.SWAGGER_2)  
//           .select()                                  
//           .apis(RequestHandlerSelectors.any())              
//           .paths(PathSelectors.any())                          
//           .build();                                           
//     }
// }
