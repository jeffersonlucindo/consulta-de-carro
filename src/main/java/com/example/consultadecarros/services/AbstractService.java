package com.example.consultadecarros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.exceptions.BusinessException;


public abstract class AbstractService {
    
    @Autowired
	MessageSource message;

    protected <T> Response<T> create(T object, String message) throws BusinessException {
        var result = new Response<T>();
        result.addReturn(object);
        result.addMessageType(message);
        return result;        
    }

    protected <T> Response<T> create(T object, String message, MessageType typeMessage) throws BusinessException {
        var result = new Response<T>();
        result.addReturn(object);
        result.addMessageType(message, typeMessage);
        return result;        
    }

    protected <T> Response<T> create(String message, MessageType typeMessage) throws BusinessException {
        var result = new Response<T>();        
        result.addMessageType(message, typeMessage);
        return result;        
    }

    protected <T> Response<T> create(Optional<T> object, String message, MessageType typeMessage) throws BusinessException {
        if(object != null && !object.isPresent()){
            throw new BusinessException(
                HttpStatus.NOT_FOUND,
                this.message.getMessage("dados.recuperacao.nao.encontrado", null, null),
                MessageType.Warning
                );
        }
        var result = new Response<T>();
        result.addReturn(object.get());
        result.addMessageType(message, typeMessage);
        return result;        
    }
}
