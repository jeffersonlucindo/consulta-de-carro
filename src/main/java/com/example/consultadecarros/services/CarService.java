package com.example.consultadecarros.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.dto.CarDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.models.CarModel;

public interface CarService {
    public Response<List<CarModel>> list()  throws BusinessException;
	public Response<CarModel> findByname(String name)  throws BusinessException;
	public Response<List<CarModel>> findByorigin(String name)  throws BusinessException;
	public Response<List<CarModel>> findBynameAndOrigin(String name, String origin)  throws BusinessException;	
	public Response<String> add(CarDto carroDTO)  throws BusinessException;
	public Response<String> delete(Long id)  throws BusinessException;
	public Boolean isExistsCar(CarDto carroDTO)  throws BusinessException;
	public Response<String> update(CarDto carroDTO)  throws BusinessException;
	public Response<CarModel> findById(String id)  throws BusinessException;
	public Response<List<CarModel>> findByCarYear(String carYear) throws BusinessException;
	public Response<Page<CarModel>> listPaginate(final Integer page, final Integer perPage) throws BusinessException;
}
