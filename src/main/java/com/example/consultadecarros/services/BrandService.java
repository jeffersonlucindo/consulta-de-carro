package com.example.consultadecarros.services;

import java.util.List;

import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.dto.BrandDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.models.BrandModel;

public interface BrandService {    
    public Response<List<BrandModel>> list()  throws BusinessException;	
	public Response<BrandModel> findByNameIgnoreCase(String name)  throws BusinessException;	
	public Response<List<BrandModel>> findByNameAndOriginIgnoreCase(String name, String origin)  throws BusinessException;	
	public Response<List<BrandModel>> findByOriginIgnoreCase( String name)  throws BusinessException;	
	public Response<String> add(BrandDto dto)  throws BusinessException;	
	public Response<String> delete(Long id)  throws BusinessException;	
	public Response<String> update(BrandDto dto, Long id)  throws BusinessException;	
	public Boolean isExistsBrand(BrandDto dto)  throws BusinessException;
}
