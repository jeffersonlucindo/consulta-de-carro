package com.example.consultadecarros.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.dto.BrandDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.models.BrandModel;
import com.example.consultadecarros.repositories.BrandRepository;
import com.example.consultadecarros.repositories.CarRepository;


@Service
public class BrandServiceImp extends AbstractService implements BrandService {

    @Autowired
    private BrandRepository repository;

    @Autowired
    private CarRepository carRepository;

    @Override
    public Response<List<BrandModel>> list() throws BusinessException {
        return create(
            repository.findAll(), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success
        );
    }

    @Override
    public Response<BrandModel> findByNameIgnoreCase(String name) throws BusinessException {
        return create(
            repository.findByNameIgnoreCase(name), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success
        );
    }

    @Override
    public Response<List<BrandModel>> findByOriginIgnoreCase(String name) throws BusinessException {
        return create(
            repository.findByOriginIgnoreCase(name), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success
        );
    }

    @Override
    public Response<String> add(BrandDto dto) throws BusinessException {
        if(dto != null && !isExistsBrand(dto)){        
            repository.save(
                BrandModel.builder()
                .name(dto.name())
                .origin(dto.origin())
                .build()                
            );
            return create(            
            this.message.getMessage("dados.salvo.sucesso", null, null),
            MessageType.Success
            );
        }
        return create(            
            this.message.getMessage("dados.recuperacao.nao.encontrado", null, null),
            MessageType.Warning
            );
    }

    @Override
    public Response<String> delete(Long id) throws BusinessException {
        var brand = repository.findById(id).get();
        if(!carRepository.findByBrand(brand).isPresent()){
            throw new BusinessException(
                HttpStatus.NOT_FOUND, 
                this.message.getMessage("dados.deletado.sucesso", null, null, null),
            null
            );
        }
        repository.deleteById(id);        
        return create(
            null, 
            this.message.getMessage("dados.deletado.sucesso", null, null),
            MessageType.Success
        );                
    }

    @Override
    public Response<String> update(BrandDto dto, Long id) throws BusinessException {
        
        var optional = repository.findById(id);
        if(optional.isPresent()){
            var brand = optional.get();
            BeanUtils.copyProperties(dto, brand);            
            repository.save(brand);
            return create(
                null, 
                this.message.getMessage("dados.atualizado.sucesso", null, null),
                MessageType.Success
                );
        }
        return create(
                null, 
                this.message.getMessage("dados.recuperacao.erro", null, null),
                MessageType.Warning
            );
    }

    @Override
    public Boolean isExistsBrand(BrandDto marcaDTO) throws BusinessException {
        var brand = repository.findByNameIgnoreCase(marcaDTO.name());
		
		return brand.isPresent();
    }

    @Override
    public Response<List<BrandModel>> findByNameAndOriginIgnoreCase(String name, String origin) throws BusinessException {
        return create(
                repository.findByNameAndOrigin(name, origin), 
                this.message.getMessage("dados.recuperacao.sucesso", null, null),
                MessageType.Success
                );
    }
    
}
