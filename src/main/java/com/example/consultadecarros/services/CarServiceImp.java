package com.example.consultadecarros.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.dto.CarDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.models.CarModel;
import com.example.consultadecarros.repositories.BrandRepository;
import com.example.consultadecarros.repositories.CarRepository;

@Service
public class CarServiceImp extends AbstractService implements CarService{

    @Autowired
    private CarRepository repository;
    
    @Autowired
    private BrandRepository brandRepository;       

    @Autowired
	private MessageSource message;

    @Override
    public Response<List<CarModel>> list() throws BusinessException {
        
        return create(
            repository.findAll(),
            this.message.getMessage("dados.recuperacao.sucesso", null, null)
            );
    }

    @Override
    public Response<CarModel> findByname(String name) throws BusinessException {

        return create(
            repository.findBynameIgnoreCase(name), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success
            );
    }

    @Override
    public Response<List<CarModel>> findByorigin(String name) throws BusinessException {
        return create(
            repository.findByoriginIgnoreCase(name), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success
            );
    }

    @Override
    public Response<String> add(CarDto dto) throws BusinessException {
        if(!isExistsCar(dto)){
            var save = new CarModel();
            if(!brandRepository.findByNameIgnoreCase(dto.brand().name()).isPresent()){
                throw new BusinessException(
                    HttpStatus.NOT_FOUND, 
                this.message.getMessage("dados.recuperacao.nao.encontrado", null, null, null), 
                null
                );
            }
            BeanUtils.copyProperties(dto, save);
            var brand = brandRepository.findByNameIgnoreCase(dto.brand().name());
            save.setBrand(brand.get());
            repository.save(save);
            return create(            
                this.message.getMessage("dados.salvo.sucesso", null, null),
                MessageType.Success
            );
        }
        return create(        
                this.message.getMessage("dados.existente", null, null),
                MessageType.Warning
            );
    }

    @Override
    public Response<String> delete(Long id) throws BusinessException {
        
        repository.deleteById(id);
        return create(            
            this.message.getMessage("dados.deletado.sucesso", null, null),
            MessageType.Success
            );
    }

    @Override
    public Response<String> update(CarDto dto) throws BusinessException {
        var optional = repository.findBynameIgnoreCase(dto.name());
        if(!optional.isPresent()){
            throw new BusinessException(
                HttpStatus.NOT_FOUND, 
                this.message.getMessage("dados.recuperacao.nao.encontrado", null, null), 
                 MessageType.Warning
                );
        }
        var car = optional.get();
        if(dto.brand() != null) {
            var brand = brandRepository.findByNameIgnoreCase(dto.brand().name()).get();
            car.setBrand(brand);
        }
        BeanUtils.copyProperties(dto, car);
        repository.save(car);
        return create(
            null, 
            this.message.getMessage("dados.atualizado.sucesso", null, null),
            MessageType.Success
            );       
    }

    @Override
    public Response<CarModel> findById(String id) throws BusinessException {
        return create(
            repository.findById(Long.parseLong(id, 0)), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null, null), 
            MessageType.Success
            );
    }

    @Override
    public Boolean isExistsCar(CarDto dto) throws BusinessException {
        var car = repository.findBynameIgnoreCase(dto.name());
		
		return car.isPresent();
    }

    @Override
    public Response<List<CarModel>> findBynameAndOrigin(String name, String origin) throws BusinessException {
        return create(
                repository.findBynameAndOrigin(name, origin), 
                this.message.getMessage("dados.recuperacao.sucesso", null, null),
                MessageType.Success
                );
    }

    @Override
    public Response<List<CarModel>> findByCarYear(String carYear) throws BusinessException {
        return create(
            this.repository.findByCarYear(carYear), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success);
    }

    @Override
    public Response<Page<CarModel>> listPaginate(final Integer page, final Integer perPage) throws BusinessException {
        final Pageable sorted = PageRequest.of(page, perPage, Sort.by("name").descending());
        return create(
            repository.findAll(sorted), 
            this.message.getMessage("dados.recuperacao.sucesso", null, null), 
            MessageType.Success);
    }
    
}
