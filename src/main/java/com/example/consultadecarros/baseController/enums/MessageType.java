package com.example.consultadecarros.baseController.enums;

public enum MessageType {
    Success,
    Info,
    Warning,
    Error,
    CriticalErro,
    Created
}
