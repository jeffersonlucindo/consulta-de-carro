package com.example.consultadecarros.baseController;


import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.baseController.models.Response;
import com.example.consultadecarros.exceptions.BusinessException;
import com.google.common.base.Strings;



public abstract class BaseContrller {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Response<?> handleValidationExceptions(MethodArgumentNotValidException exception) {        
        var response = new Response<>();        
        exception.getBindingResult().getAllErrors().forEach((error) -> {
            final String fieldName = ((FieldError) error).getField();
            final String errorMessage = error.getDefaultMessage();
            response.addError(
              "O campo '" + fieldName +"' "+errorMessage, 
              MessageType.Error);            
        });
                
        return response;

        // return errors;
    }

    protected ResponseEntity<?> processAndLog(Response<?> response) throws BusinessException{
      try
      {
        return ResponseEntity.status(HttpStatus.OK).body(response);
      }
      catch (Exception e)
      {
        throw new BusinessException(e.getMessage());
      }    
   }

   protected Boolean isBlankString(String text){
    
    return Strings.isNullOrEmpty(text);
   }
    
}
