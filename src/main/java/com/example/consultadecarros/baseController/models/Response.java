package com.example.consultadecarros.baseController.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.exceptions.BusinessException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Response<T> {

    private List<MessageResult> messages = new ArrayList<MessageResult>();
    @JsonInclude(Include.NON_NULL)
    private T body;

    public void addMessageType(String message, MessageType type) throws BusinessException {        
        validate(message);
        this.messages.add(
            MessageResult
                .builder()
                .message(message)
                .type(type)
                .build()
            );        
    }

    public void addMessageType(String message) throws BusinessException {        
        validate(message);
        this.messages.add(
            MessageResult
                .builder()
                .message(message)
                .type(MessageType.Success)
                .build()
            );        
    }

    public void addReturn(T result) throws BusinessException {
        validate(result);        
        this.body = result;
    }

    public void addError(String message){
        this.messages.add(
            MessageResult
                .builder()
                .message(message)
                .type(MessageType.Error)
                .build()
            );         
    }

    public void addError(String message, MessageType type){
        this.messages.add(
            MessageResult
                .builder()
                .message(message)
                .type(type)
                .build()
            );         
    }
    private void validate(Object ob) throws BusinessException{
        if(ob.equals(null) ) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "Object is empty");
        }
    }
}
