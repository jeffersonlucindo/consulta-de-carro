package com.example.consultadecarros.baseController.models;

import com.example.consultadecarros.baseController.enums.MessageType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MessageResult {
    private String message;
    private MessageType type;
}
