package com.example.consultadecarros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaDeCarrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaDeCarrosApplication.class, args);
	}

}
