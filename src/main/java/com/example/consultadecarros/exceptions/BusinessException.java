package com.example.consultadecarros.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.consultadecarros.baseController.enums.MessageType;
import com.example.consultadecarros.baseController.models.Response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BusinessException extends Exception {

    private final HttpStatus statusMessage;
    private final String message;        
    private final MessageType typeMessage;

    public BusinessException(HttpStatus statusMessage, String message) {
        super(message);
        this.statusMessage = statusMessage;        
        this.message = message;
        this.typeMessage = null;        
    }

    public BusinessException(String message) {
        super(message);
        this.statusMessage = HttpStatus.BAD_REQUEST;
        this.message = message;        
        this.typeMessage = null;
    }    

    public BusinessException(HttpStatus statusMessage, String message, MessageType typeMessage) {
        super(message);
        this.statusMessage = statusMessage;
        this.message = message;        
        this.typeMessage = typeMessage;
    }
}
