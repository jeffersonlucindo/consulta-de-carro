package com.example.consultadecarros.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.example.consultadecarros.baseController.models.Response;

@ControllerAdvice
public class ApiExceptionHendler extends ResponseEntityExceptionHandler {
    
   @ExceptionHandler({BusinessException.class})
    public ResponseEntity<?> handleBusinessException(BusinessException ex){

        return ResponseEntity.status(ex.getStatusMessage()).body(buildBodyError(ex));
    }

    private Object buildBodyError(BusinessException ex) {
        var response = new Response<>();
        if(ex.getTypeMessage() != null){
            response.addError(ex.getMessage(), ex.getTypeMessage());
        } else {
            response.addError(ex.getMessage());
        }
        return response;
    }   
    
}
