package com.example.consultadecarros.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.consultadecarros.models.BrandModel;
import com.example.consultadecarros.models.CarModel;
import java.util.List;


@Repository
public interface CarRepository extends BaseRepository<CarModel, Long> {
    
    // @Override
    // public Optional<List<CarModel>> findAll();

    public Optional<CarModel> findBynameIgnoreCase(String name);

    public Optional<List<CarModel>> findByoriginIgnoreCase(String origin);

    public List<CarModel> findBynameAndOrigin(String name, String origin);

    public Optional<CarModel> findByBrand(BrandModel brand);

    @Query(name = "select * from car where car_year = :carYear", nativeQuery = true)
    public Optional<List<CarModel>> findByCarYear(String carYear);
}
