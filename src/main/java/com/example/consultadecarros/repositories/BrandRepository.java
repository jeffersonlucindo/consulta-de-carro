package com.example.consultadecarros.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.consultadecarros.models.BrandModel;

@Repository
public interface BrandRepository extends BaseRepository<BrandModel, Long> {
    
    public Optional<BrandModel> findByNameIgnoreCase(String name);

    public Optional<List<BrandModel>> findByOriginIgnoreCase(String origin);

    public List<BrandModel> findByNameAndOrigin(String name, String origin);
}
