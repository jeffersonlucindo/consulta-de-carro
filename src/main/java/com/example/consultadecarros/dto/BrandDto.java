package com.example.consultadecarros.dto;

import jakarta.validation.constraints.NotNull;

public record BrandDto(
    @NotNull(message = "não pode ser nulo")
    String name, 
    @NotNull(message = "não pode ser nulo")
    String origin
    ) {}
