package com.example.consultadecarros.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record CarDto(
	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo") 
	String name,
	
	@NotNull(message = "não pode ser nulo")
	Long kmPerGallon,

	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo")
	String cylinders,

	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo")
	String gallowsHorses,
	
	@NotNull(message = "não pode ser nulo")
	Long weight,

	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo")
	String acceleration,

	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo")
	String year,

	@NotBlank(message = "não pode ser em branco")
	@NotNull(message = "não pode ser nulo")
	String origin,

	@NotNull(message = "não pode ser nulo")
	BrandDto brand
	 ) {}
