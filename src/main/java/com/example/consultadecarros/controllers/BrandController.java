package com.example.consultadecarros.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.consultadecarros.baseController.BaseContrller;
import com.example.consultadecarros.dto.BrandDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.services.BrandService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("brands")
public class BrandController extends BaseContrller {
    
    @Autowired
    private BrandService service;

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<?> listar(
        @RequestParam(name = "name", required = false)  String name,
        @RequestParam(name = "origin", required = false)  String origin        
        ) throws BusinessException {
            if(!isBlankString(name)) 
                return processAndLog(service.findByNameIgnoreCase(name));
            
            if(!isBlankString(origin))
                return processAndLog(service.findByOriginIgnoreCase(origin));
            
            if(!isBlankString(origin) && !isBlankString(name)) 
                return  processAndLog(service.findByNameAndOriginIgnoreCase(name, origin));
            
            return processAndLog(service.list());
    }
   
    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> adicionar(
        @NotNull 
        @RequestBody 
        BrandDto marcaDTO
        ) throws BusinessException {
        return processAndLog(service.add(marcaDTO));
    }

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletar(@PathVariable("id") Long id) throws BusinessException {
        return processAndLog(service.delete(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> atualizar(
        @RequestBody BrandDto marcaDTO,
        @PathVariable("id") Long id
        ) throws BusinessException {
        return processAndLog(service.update(marcaDTO, id));
    }
}
