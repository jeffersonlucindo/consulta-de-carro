package com.example.consultadecarros.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.consultadecarros.baseController.BaseContrller;
import com.example.consultadecarros.dto.CarDto;
import com.example.consultadecarros.exceptions.BusinessException;
import com.example.consultadecarros.services.CarService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("cars")
public class CarController extends BaseContrller {
    
    @Autowired
    private CarService service;

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<?> listar(
        @RequestParam(name = "name", required = false) String name,
        @RequestParam(name = "origin", required = false) String origin   
    ) throws BusinessException {        
		if(!isBlankString(name)) 
                return processAndLog(service.findByname(name));
            
            if(!isBlankString(origin))
                return processAndLog(service.findByorigin(origin));
            
            if(!isBlankString(origin) && !isBlankString(name)) 
                return  processAndLog(service.findBynameAndOrigin(name, origin));
            
            return processAndLog(service.list());
	}

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findById(@PathVariable("id") String id) throws BusinessException {
        return processAndLog(service.findById(id));
    }  

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> adicionar(@Valid @RequestBody CarDto carroDTO) throws BusinessException {
        return processAndLog(service.add(carroDTO));
    }

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletar(@PathVariable("id") Long id) throws BusinessException {
        return processAndLog(service.delete(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<?> atualizar(@RequestBody CarDto carroDTO) throws BusinessException {
        return processAndLog(service.update(carroDTO));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/caryear/{year}", method = RequestMethod.GET)
	public ResponseEntity<?> findByCarYear(@PathVariable("year") String carYear) throws BusinessException {
        return processAndLog(service.findByCarYear(carYear));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/paginated", method = RequestMethod.GET)
	public ResponseEntity<?> listPaginate(
        @RequestParam(defaultValue = "0") final Integer page,
        @RequestParam(defaultValue = "2") final Integer perPage
    ) throws BusinessException {
        return processAndLog(service.listPaginate(page, perPage));
    }
}
