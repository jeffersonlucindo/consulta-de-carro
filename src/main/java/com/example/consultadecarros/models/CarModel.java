package com.example.consultadecarros.models;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity(name = "car")
public class CarModel implements Serializable {
 
    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long carId;
	
	private String name;
	private Long kmPerGallon;
	private String cylinders;
	private String gallowsHorses;
	private Long weight;
	private String acceleration;
	private String carYear;
	private String origin;
	
	@ManyToOne(fetch = FetchType.EAGER )
    @JoinColumn(name = "idBrand", nullable = false)
    private BrandModel brand;
}
/**
 * private String name;
	private Long kmPerGallon;
	private String cylinders;
	private String gallowsHorses;
	private Long weight;
	private String acceleration;
	private String year;
	private String origin;
 */