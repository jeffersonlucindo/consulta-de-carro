insert into brand (name, origin) values ('TOYOTA','DF');
insert into brand (name, origin) values ('HYUNDAI','DF');
insert into brand (name, origin) values ('GM - CHEVROLET','DF');
insert into brand (name, origin) values ('MERCEDES','DF');
insert into brand (name, origin) values ('CITROEN','DF');
insert into brand (name, origin) values ('FIAT','DF');
insert into brand (name, origin) values ('FORD','DF');
insert into brand (name, origin) values ('KIA MOTORS','DF');

insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 1, 'Corolla', 'DF', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 2, 'HB20', 'GO', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 3, 'ONIX', 'DF', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 4, 'SLK-200', 'RJ', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 5, 'PEGEOT 208', 'DF', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 6, 'UNO', 'SP', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 7, 'FIESTA', 'AM', 3504);
insert into car (acceleration, car_year, gallows_horses, cylinders, km_per_gallon, id_brand, name, origin, weight) 
values ( '12', '1970-01-01', '130', 8, 18, 8, 'SPORTAGE', 'DF', 3504);