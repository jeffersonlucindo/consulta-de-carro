## Realizar o build

- mvn clean install

## Para rodar o projeto

- mvn spring-boot:run

## Para conectar no H2 database

- Acessar url do navegador http://localhost:8080/api-carros/h2-console
- Incluir em JDBC URL: jdbc:h2:mem:consulta-carros
- user=sa
- password=sa

## EndPoints de cars

- (GET)/api-carros/cars/list;
- (GET)/api-carros/cars/paginated;
- (POST/)api-carros/cars/add;
- (PUT)/api-carros/cars/update/;
- (GET)/api-carros/cars/caryear/{year};
- (DELETE)/api-carros/cars/delete/{id}

## EndPoints de brand

- (GET)/api-carros/brands/list
- (POST)/api-carros/brands/add
- (PUT)/api-carros/brands/update/{id}
- (DELETE)/api-carros/brands/delete/{id}
